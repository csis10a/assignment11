
/**
 * Write a description of class Rational here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rational
{
    // 1) define instance variables here


    
    // 2) define the no-arg constructor here
    
       
    // 3) define the printRational method here
    
    
    
    public static void main(String [] args){
       /*********** Problem 4 ******************************
       // 4) Verify your class works to this point
        
       //  a) create a Rational number using your no-arg constructor
       
       //  b) set the numerator directly to 3
       
       //  c) set the denominator directly to 4
       
       //  d) print the Rational number
       
       
       //***************** end problem 4 ********************/
       /************ Problem 5 ******************************
       // 5) Add a second, parameter constructor above main and test it here
       //    (see assignment11 handout)  
       
       Rational num2 = new Rational(12, 15);
       printRational(num2);
       
       //***************** end problem 5 ********************/
       /************ Problem 6 ******************************
       // 6) Add a method called negate above main and test it here
       // (see assignment11 handout)
       
       Rational num3 = new Rational(20,25);
       negate(num3);
       printRational(num3);
       
       //***************** end problem 6 ********************/
       /************ Problem 7 ******************************
       // 7) Add a method called invert above main and test it here
       // (see assignment11 handout)

       
       //***************** end problem 7 ********************/
       /************ Problem 8 ******************************
       // 8) Write a method called toDouble above main and test it here
       //    (see assignment11 handout)
       
       
       //***************** end problem 8 ********************/
       /************ Problem 9 ******************************
       // 9) Write a method called reduce above main that reduces a rational  
       //  number to its lowest terms and test it here (see assignment11 handout)
       
       // here is a statement that demonstrates the findGCD method
       // you will have to type in findGCD above main for this to work
       
       System.out.println("Greatest common divisor of 24 and 54 is " + findGCD(24,54));

       // you will call this method in your reduce method to find the GCD of 
       // of the numerator and denominator
       
       
       
       //***************** end problem 9 ********************/
       /************ Problem 10 ******************************
       // 10) Write a method called add above main and test it here
       //    (see assignment11 handout)
       
       
       
       
       //***************** end problem 10 ********************/

    }
}
