# CSIS 10A - Assignment 11

__Prerequisite: Lecture or Reading on Chapter 11__

Clone or download and extract the Assignment11 to your workspace. Then open the  project.blueJ file to begin the exercises below.  

A rational number is a number that can be represented as the ratio of two integers. Rational numbers are also known as fractions. For example, 2/3 is a rational number, and you can think of 7 as a rational number with an implicit 1 in the denominator. For this assignment, you are going to write a class definition for rational numbers.

1. In the assignment 11 download, you’ll find a class called `Rational`. First, define the instance variables for the class. a `Rational` object should have two integer instance variables to store the numerator and denominator.   
3. Next, write a constructor that takes no arguments and that sets the numerator to 0 and denominator to 1.   
4. Write a method called `printRational` that takes a Rational object as an argument and prints it in some reasonable format.   
5. Write a `main` method that creates a new object with type Rational using your no-arg constructor, then sets its instance variables directly to represent the fraction 3/4, and prints the object.  At this stage, you have a minimal testable program. Test it and, if necessary, debug it.   
6. Write a second constructor for your class that takes two arguments and that uses them to initalize the instance variables. Test this out in main using the statement `Rational num2 = new Rational(12, 15);`  and print the value of `num2 `  
7. Write a method called negate that reverses the sign of a rational number (by changing the sign of the numerator). This method should be a modifier, so it should return void. Add lines to main to test the new method.   
8. Write another modifier method called `invert` that inverts the number by swapping the numerator and denominator. Add lines to main to test the new method.   
9. Write a method called `toDouble` that converts the rational number to a `double` (floating-point number) and returns the result. This method is a pure function; it does not modify the object. As always, test the new method.   
10. Write a modifier named `reduce` that reduces a rational number to its lowest terms by finding the greatest common divisor (GCD) of the numerator and denominator and dividing through. For example, if `Rational num = new Rational(54, 24);`  then the call  `reduce(num);` will change `num` to the value 9/4 (by dividing both numerator and denominator by 6) . To find the GCD, use the following method by typing it into your Rational file above main:
```
#!java
    private static int findGCD(int number1, int number2) {
        //base case
        if(number2 == 0){
            return number1;
        }
        return findGCD(number2, number1%number2);
    }
```
- Test statements are included in main that you can run to see how the `findGCD` method words. In your `reduce` method, you will have to pass the numerator and denominator of the Rational object to `findGCD` to find the greatest common divisor. Then divide both numerator and denominator by that number.  
10. Write a method called `add` that takes two `Rational` numbers as arguments and returns a new `Rational` object. The return object should contain the sum of the arguments. There are several ways to add fractions, see [http://www.mathplayground.com/fractions_add.html](http://www.mathplayground.com/fractions_add.html) for one example.
You can use any one you want, but you should make sure that the result of the operation is reduced so that the numerator and denominator have no common divisor (other than 1).  


When you are finished, make a jar and upload your project to MPC Online.
